r-cran-rnetcdf (2.9-2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.2 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Mon, 10 Mar 2025 14:02:23 +0900

r-cran-rnetcdf (2.9-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 18 Jan 2024 13:41:32 +0100

r-cran-rnetcdf (2.8-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 23 Oct 2023 14:10:49 +0200

r-cran-rnetcdf (2.6-2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 21 Jan 2023 15:17:28 +0100

r-cran-rnetcdf (2.6-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 29 Jun 2022 08:47:26 +0200

r-cran-rnetcdf (2.5-2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 27 Aug 2021 22:14:00 +0200

r-cran-rnetcdf (2.4-2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Set upstream metadata fields: Bug-Submit.
  * Remove broken lintian-override

 -- Andreas Tille <tille@debian.org>  Tue, 22 Sep 2020 08:55:29 +0200

r-cran-rnetcdf (2.3-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Thu, 07 May 2020 10:41:51 +0200

r-cran-rnetcdf (2.1-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g

 -- Andreas Tille <tille@debian.org>  Tue, 12 Nov 2019 14:18:25 +0100

r-cran-rnetcdf (2.0-3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database.
  * Remove obsolete fields Name from debian/upstream/metadata.

 -- Andreas Tille <tille@debian.org>  Fri, 11 Oct 2019 11:02:28 +0200

r-cran-rnetcdf (1.9-1-2) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Remove unneeded get-orig-source target

 -- Andreas Tille <tille@debian.org>  Thu, 21 Jun 2018 16:39:46 +0200

r-cran-rnetcdf (1.9-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Secure URI in watch file
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Tue, 24 Oct 2017 16:47:39 +0200

r-cran-rnetcdf (1.8-2-1) unstable; urgency=low

  * Team upload

  [ Sebastian Gibb ]
  * New upstream release.
  * debian/control:
    - set Standards-Version: 3.9.8.
    - use https:// in Vcs-Git and Vcs-Browser.
    - use secure canonical URL in Homepage.
    - set minimal r-base-dev version to 2.5.0.
  * debian/copyright: remove entry for inst/HDF5_COPYING because this file
    was removed upstream.
  * debian/patches: removed hardening patches.

  [ Andreas Tille ]
  * cme fix dpkg-control
  * Convert to dh-r
  * debhelper 10
  * d/watch: version=4
  * update d/copyright
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Tue, 29 Nov 2016 19:20:05 +0100

r-cran-rnetcdf (1.6.3-1-1) unstable; urgency=low

  [ Debian Science Maintainers ]
  * New upstream release.
  * Move debian/upstream to debian/upstream/metadata.
  * debian/copyright:
    - Swap first (Files: man/*) and second (Files: *) entry to
      avoid the unused-file-paragraph-in-dep5-copyright lintian warning.
    - Change license name from "NetCDF (MIT like)" to "NetCDF" and "NCSA HDF5"
      to "NCSA-HDF5" to avoid the space-in-std-shortname-in-dep5-copyright
      lintian warning.

  [ Filippo Rusconi ] <lopippo@debian.org>
  * Upload

 -- Filippo Rusconi <lopippo@debian.org>  Wed, 01 Oct 2014 09:23:58 +0200

r-cran-rnetcdf (1.6.2-3-1) unstable; urgency=low

  [ Debian Science Maintainers ]
  * New upstream release.

  [ Filippo Rusconi ]
  * copyright: include full text of the NCSA HDF5 license.
  * Upload (lopippo@debian.org).

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 23 Jun 2014 13:15:30 +0200

r-cran-rnetcdf (1.6.2-2-1) unstable; urgency=low

  * New upstream release.

 -- Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>  Tue, 27 May 2014 11:02:06 +0200

r-cran-rnetcdf (1.6.1-2-1) unstable; urgency=low

  * Initial release. (Closes: #737533);
  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>  Mon, 10 Feb 2014 15:16:45 +0100
